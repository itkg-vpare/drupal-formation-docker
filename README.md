# Pré-requis
Cloner le repository git drupal, cf. https://www.drupal.org/project/drupal/git-instructions :
```bash
git clone --branch 8.6.x https://git.drupal.org/project/drupal.git
```

À la racine du repository drupal, créer le dossier translations :
```bash
mkdir -vp sites/default/files/translations
```

Puis cloner ce repo contenant l'environnement docker :
```bash
git clone git@gitlab.com:itkg-vpare/drupal-formation-docker.git
```

# Installation
Toutes les commandes sont à exécuter dans le dossier racine de ce repo (drupal-formation-docker).

## Édition docker-compose.yml
Copier le fichier `docker-compose.yml.dist` :
```bash
cp -v docker-compose.yml.dist docker-compose.yml
```

Modifier (si nécessaire) le chemin des volumes pour cibler le bon dossier sur votre machine.

Lancer la construction de la stack :
```bash
docker-compose up -d --build
```

## Configuration MySQL
```bash
# Entrer dans le conteneur MySQL
docker-compose exec db bash

# Créer la base drupal
mysql -u root -proot -e "CREATE DATABASE drupal;"
```

## Composer install
```bash
docker-compose exec app composer install
```

## Fichier hosts
```
192.168.56.101 drupal8.local.io
```
(remplacer par l'IP par celle du docker host)

## Installation de drupal
Lancer l'installation via http://drupal8.local.io/

# Utilisation
- Drupal : http://drupal8.local.io/
- Adminer : http://drupal8.local.io:8080/?server=db&username=root&db=drupal
- Mailcatcher : http://drupal8.local.io:1080/

### Identifants de connexion MySQL
- host : `db`
- port : `3306`
- user : `root`
- pass : `root`

### Drush
Drush est installé dans le conteneur `app`. On peut utiliser une fonction bash (ou un alias) pour simplifier son utilisation :

```bash
function drush { docker-compose -f ../drupal-formation-docker/docker-compose.yml exec app drush "$@"; }
```
