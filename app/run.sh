#!/usr/bin/env sh

chown -R www-data:www-data sites
socat TCP-LISTEN:25,fork TCP:mailcatcher:1025 &
exec apache2-foreground
